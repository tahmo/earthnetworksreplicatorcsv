# Import dependencies.
import EarthNetworks
from utility import columnMapping
import schedule
import datetime
from dateutil.parser import parse
from dateutil.relativedelta import relativedelta
import time
import os
import csv
from models import Base, Station
from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker
import pandas as pd

# Load environment variables from file.
from dotenv import load_dotenv
load_dotenv()

# Initialize database session.
db_uri = os.getenv('SQLALCHEMY_DATABASE_URI')
engine = create_engine(os.getenv('DB_URI'))
Session = sessionmaker(bind=engine)

# Create all database tables.
Base.metadata.create_all(engine)

# Initialize EarthNetworks API.
api = EarthNetworks.apiWrapper()
api.setCredentials(os.getenv('EN_API_KEY'))


def updateMeasurements():
 print('Update measurements')
 stations = ['TMSNA','TLGNR','MHJNG','ANTNN','FNRNT','ANTSR']
 for station in stations:
  updateMeasurementsForStation(station)


def updateMeasurementsForStation(stationCode):
 print('Update measurements for station %s' % stationCode)
 session = Session()
 try:
  df = api.getMeasurements(stationCode)
  if len(df):
   # CSV export.
   df.index.name = 'timestamp'
   drop_columns = []
   for columnName in list(df):
    if columnName not in columnMapping:
     drop_columns.append(columnName)
    else:
     df.rename({columnName: columnMapping[columnName]['name']}, axis=1, inplace=True)

   # Drop unused columns.
   df.drop(columns=drop_columns, axis=1, inplace=True)

   # Order columns alphabetically.
   df = df.reindex(columns=sorted(df.columns))

   filename = 'data/%s.csv' % (stationCode)

   if os.path.isfile(filename):
    # Save the dataframe to a CSV file.p
    df.to_csv(filename, mode='a', header=False, na_rep=os.getenv('NA_VALUE'),
              date_format=os.getenv('DATE_FORMAT'))
   else:
    # Save the dataframe to a CSV file.p
    df.to_csv(filename, na_rep=os.getenv('NA_VALUE'),
              date_format=os.getenv('DATE_FORMAT'))

   if os.getenv('CURRENT_FILE_ROWS') != '':
    # Save last rows to the current file.
    df_full = pd.read_csv(filename)
    df_full.tail(int(os.getenv('CURRENT_FILE_ROWS'))).to_csv('data/%s_current.csv' % (stationCode),
                                                       na_rep=os.getenv('NA_VALUE'),
                                                       date_format=os.getenv('DATE_FORMAT'))

   session.commit()

 except Exception as e:
  print('Error while retrieving measurements for station %s: %s' % (stationCode, str(e)))
 finally:
  print('Finished updating measurements for station %s' % stationCode)
  session.close()


# Schedule periodic update jobs.
print('Scheduling jobs')
schedule.every(1).hour.at(":05").do(updateMeasurements)

# Manually trigger updates for testing purposes.
updateMeasurements()

while True:
 schedule.run_pending()
 time.sleep(1)
