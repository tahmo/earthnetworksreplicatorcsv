# Module dependancies
import requests
import pandas as pd

# Module Constants
API_BASE_URL = 'https://earthnetworks.azure-api.net'

class apiWrapper(object):
    apiKey = ''

    def setCredentials(self, key):
        self.apiKey = key

    def getMeasurements(self, station, startDate=None, endDate=None):
        print('Get measurements', station, startDate, endDate)
        endpoint = 'data/observations/v4/current'

        params = { 'stationid': station, 'providerid': 3, 'units': 'metric', 'verbose': 'true', 'metadata': 'true'}
        response = self.__request(endpoint, params)
        if 'observation' in response:
            print (response['observation']['observationTimeUtcStr'])

            df = pd.DataFrame(
                {
                    'humidity': [response['observation']['humidity']],
                    'temperature': [response['observation']['temperature']],
                    'windSpeed': [response['observation']['windSpeed']],
                    'windDirection': [response['observation']['windDirection']],
                    'pressureSeaLevel': [response['observation']['pressureSeaLevel']]
                },
                index = pd.DatetimeIndex([response['observation']['observationTimeUtcStr']])
            )
            return df
        else:
            return pd.DataFrame()


    def __request(self, endpoint, params):
        print('API request: %s' % endpoint)
        params['subscription-key'] = self.apiKey
        apiRequest = requests.get(
            '%s/%s' % (API_BASE_URL, endpoint),
            params=params
        )

        if apiRequest.status_code == 200:
            return apiRequest.json()
        else:
            return self.__handleApiError(apiRequest)

    def __handleApiError(self, apiRequest):
        json = None
        try:
            # Try to parse json and check if body contains a specific error message.
            json = apiRequest.json()
        finally:
            if json and 'error' in json and 'message' in json['error']:
                raise Exception(json['error']['message'])
            else:
                raise Exception('API request failed with status code %s' % apiRequest.status_code)
