columnMapping = {
    'temperature': {
        'name': 'AirTemperatureAVG',
        'units': 'degrees C'
    },
    'humidity': {
        'name': 'RH',
        'units': '%'
    },
    'windSpeed': {
        'name': 'WindSpeed',
        'units': 'm/s'
    },
    'windDirection': {
        'name': 'WindDirection',
        'units': 'degrees'
    },
    'pressureSeaLevel': {
        'name': 'AtmosphericPressureMSL',
        'units': 'hPa'
    }
}